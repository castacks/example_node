/* An example node using the BaseNode class.
 *
 * author: John Keller jkeller2@andrew.cmu.edu
 *
 */
#include <base/BaseNode.h>
#include <sensor_msgs/Image.h>
#include <string>
#include "example_node/example_node.h"
#include <stdlib.h>
#include <time.h>

ImageNode::ImageNode(std::string node_name)
  : BaseNode(node_name)
  , new_image(false)
  , count(0){
  // unused array to show an exampe of deinitialization in the deconstructor
  example_array = new int[100];
}

/*!
 * \brief Callback that saves the current image.
 */
void ImageNode::image_callback(sensor_msgs::Image image){
  current_image = image;
  new_image = true;
}

/*!
 * \brief Initialize ROS related objects here. Get a pointer to the node handle
 * and create a publisher and subscriber.
 */
bool ImageNode::initialize(){
  ros::NodeHandle* nh = get_node_handle();

  image_sub = nh->subscribe("/camera/image_raw", 10, &ImageNode::image_callback, this);
  blue_image_pub = nh->advertise<sensor_msgs::Image>("/node/camera/image_blue", 10);

  // Check if a parameter exists.
  // If it doesn't the initialize function returns false, indicating failure.
  ros::NodeHandle* pnh = get_private_node_handle();
  int value;
  bool found_parameter = pnh->getParam("parameter_example", value);
  if(!found_parameter)
    return false;

  return true;
}

/*!
 * \brief If there is a new image, make it blue, and publish it.
 */
bool ImageNode::execute(){
  // Make each pixel a little more blue and publish.
  if(new_image){
    for(int i = 2; i < current_image.data.size(); i += 3)
      current_image.data[i] = std::min(current_image.data[i] + 100, 255);
    new_image = false;
    blue_image_pub.publish(current_image);
    count++;
  }

  // Shutdown the node after processing 100 images.
  if(count == 100){
    ROS_INFO_STREAM("100 images have been processed. Shutting down " << get_node_name() << "." );

    // Fail half of the time.
    srand(time(NULL));
    if(rand()%2 == 0)
      fail("Example of a failure.");

    // Returning false indicates that execute should stop being called.
    return false;
  }

  // Returning true indicates that execute will keeping being called.
  return true;
}

/*!
 * \brief Do any cleanup here.
 */
ImageNode::~ImageNode(){
  // delete the array
  delete[] example_array;
}

/*!
 * \brief Return an instance of our node.
 * This is so the main function can start executing the node.
 * get() is guaranteed to only be called once.
 */
BaseNode* BaseNode::get(){
  ImageNode* image_node = new ImageNode("ImageNode");
  return image_node;
}
