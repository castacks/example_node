This package is an example of a BaseNode. Comments on how to derive from BaseNode and use each function can be found in src/example_node.cpp.


Author: John Keller jkeller2@andrew.cmu.edu slack: kellerj
