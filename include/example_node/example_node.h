#ifndef _EXAMPLE_NODE_H_
#define _EXAMPLE_NODE_H_

#include <base/BaseNode.h>
#include <sensor_msgs/Image.h>
#include <string>


class ImageNode : public BaseNode {
private:
  bool new_image;
  int count;
  sensor_msgs::Image current_image;
  ros::Subscriber image_sub;
  ros::Publisher blue_image_pub;
  int* example_array;

public:
  ImageNode(std::string node_name);
  void image_callback(sensor_msgs::Image image);

  virtual bool initialize();
  virtual bool execute();
  virtual ~ImageNode();

};


#endif
